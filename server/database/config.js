const Sequelize = require("sequelize");
const sequelize = new Sequelize("TrialDB", "sa", "sa", {
  host: "NIDHOGG",
  dialect: "mssql",

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});

module.exports.defaultConfig = {
  timestamps: false,
  paranoid: false,
  underscored: false,
  freezeTableName: true,
  version: false
};

module.exports.db = sequelize;
