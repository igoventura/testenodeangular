import { Sequelize } from "sequelize";
import { db, defaultConfig } from "../database/config";

const sequelize = db;

var CourseModel = sequelize.define(
  "Course",
  {
    CourseID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    CourseName: Sequelize.STRING,
    CourseDescription: Sequelize.STRING
  },
  defaultConfig
);

var ProfileModel = sequelize.define(
  "Profile",
  {
    ID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    ProfileName: Sequelize.STRING
  },
  defaultConfig
);

var UsersModel = sequelize.define(
  "Users",
  {
    ID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    Username: {
      type: Sequelize.STRING,
      allowNull: false
    },
    ProfileID: {
      type: Sequelize.INTEGER,

      references: {
        model: ProfileModel,

        key: "ID"
      }
    }
  },
  defaultConfig
);

UsersModel.belongsTo(ProfileModel);

module.exports.Course = CourseModel;

module.exports.Profile = ProfileModel;

module.exports.Users = UsersModel;
