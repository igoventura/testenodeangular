import {
    Course
} from '../models/Models';

module.exports.CourseController = {
    defineMethods: function (router) {

        router.get("/course", function(req, res) {
          Course.findAll()
            .then(courses => {
              return courses;
            })
            .then(list => {
              res.send(list);
            });
        }).post("/course", function(req, res) {
          Course.create({
            CourseName: req.body.courseName,
            CourseDescription: req.body.courseDescription
          })
            .then(result => result)
            .then(data => {
              res.send(data);
            });
        }).put("/course", function(req, res) {
          Course.findById(req.body.courseId)
            .then(course => {
              course.CourseName = req.body.courseName;
              course.CourseDescription = req.body.courseDescription;

              return course.save();
            })
            .then(result => {
              res.send(result);
            })
            .catch(err => {
              res.send(err);
            });
        }).delete("/course", function(req, res) {
          Course.findById(req.body.id)
            .then(course => {
              return course.destroy();
            })
            .then(result => {
              res.send(result);
            })
            .catch(err => {
              res.send(err);
            });
        });
    }
};