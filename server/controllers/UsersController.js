import { Users, Profile } from "../models/Models";

module.exports.UsersController = {
  defineMethods: function(router) {
    router
      .get("/user", function(req, res) {
        Users.findAll({ include: [{ model: Profile }] })
          .then(users => users)
          .then(list => {
            res.send(list);
          });
      })
      .post("/user", function(req, res) {
        Users.create({
          Username: req.body.userName,
          ProfileID: req.body.profileId
        })
          .then(result => result)
          .then(data => {
            res.send(data);
          });
      })
      .get("/user/:user_id", function(req, res) {
        Users.findById(req.params.user_id, {
          include: [{ model: Profile }]
        })
          .then(result => result)
          .then(user => {
            res.send(user);
          })
          .catch(err => {
            res.send(err);
          });
      })
      .put("/user/:user_id", function(req, res) {
        Users.findById(req.params.user_id)
          .then(user => {
            user.Username = req.body.userName;
            user.ProfileID = req.body.profileId;

            return user.save();
          })
          .then(result => {
            res.send(result);
          })
          .catch(err => {
            res.send(err);
          });
      })
      .delete("/user/:user_id", function(req, res) {
        Users.findById(req.params.user_id)
          .then(user => {
            return user.destroy();
          })
          .then(result => {
            res.send(result);
          })
          .catch(err => {
            res.send(err);
          });
      });
  }
};
