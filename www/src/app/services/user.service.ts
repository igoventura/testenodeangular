import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { map, catchError } from 'rxjs/operators';

import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class UserService {
  constructor(private http: Http) {}

  // createCourse(course: any) {
  //   return this.http
  //     .post('http://localhost:8080/api/CreateCourse/', course)
  //     .map((response: Response) => response.json());
  // }

  // updateCourse(course: any) {
  //   return this.http
  //     .put('http://localhost:8080/api/UpdateCourse/', course)
  //     .map((response: Response) => response.json());
  // }

  getAllUsers() {
    return this.http.get('http://localhost:8080/api/user').map((response: Response) => response.json());
  }

  deleteUser(id: number) {
    return this.http.delete(`http://localhost:8080/api/user/${id}`).map((response: Response) => response.json());
  }
}
