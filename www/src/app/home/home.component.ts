import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { UserService } from '../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  users: Array<any>;
  isLoading: boolean;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.isLoading = true;
    this.userService.getAllUsers().subscribe(data => {
      this.users = data;
      this.isLoading = false;
    });
  }

  deleteUser(id: number) {
    this.isLoading = true;
    this.userService.deleteUser(id).subscribe(data => {
      const idx = this.users.indexOf(this.users.filter(u => u.ID === id)[0]);

      if (idx >= 0) {
        this.users.splice(idx, 1);
      }
      this.isLoading = false;
    });
  }
}
